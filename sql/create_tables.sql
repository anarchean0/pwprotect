create table users(
       id serial not null,
       "name" varchar(256) not null,
       pw_hash_salt varchar(90) not null,
       pw_hash varchar(90) not null,
       secret_iv varchar(25) not null,
       secret_key varchar(50) not null,
       iterations integer not null,

       constraint users_pk_id primary key (id),
       constraint name_unique unique ("name")
);

create table passwords(
       id serial not null,
       "name" varchar(512) not null,
       value varchar(512) not null,
       user_id integer not null,
       iv varchar(25) not null,
       tag_list varchar(4096) not null,

       constraint passwords_pk_id primary key(id),
       constraint passwrods_users_fk
              foreign key (user_id) references users(id)
			  on delete cascade
			  on update cascade
);
