package pwprotect.model;

public class EncryptedPassword {

    private Long id;
    private String name;
    private String value;
    private User user;
    private String iv;
    private String tagList;

    public EncryptedPassword(Long id, String name, String value, User user, String iv, String tagList) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.user = user;
        this.iv = iv;
        this.tagList = tagList;
    }

    public EncryptedPassword(String name, String value, User user, String iv, String tagList) {
        this.id = null;
        this.name = name;
        this.value = value;
        this.user = user;
        this.iv = iv;
        this.tagList = tagList;
    }

    public EncryptedPassword() {
        this.id = null;
        this.name = null;
        this.value = null;
        this.user = null;
        this.iv = null;
        this.tagList = null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    public String getTagList() {
        return tagList;
    }

    public void setTagList(String tagList) {
        this.tagList = tagList;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof EncryptedPassword)) {
            return false;
        }

        EncryptedPassword pw = (EncryptedPassword) other;

        return (id == null ? pw.id == null : id.equals(pw.id));
    }

    @Override
    public int hashCode() {
        return id == null ? 0 : id.hashCode();
    }
}
