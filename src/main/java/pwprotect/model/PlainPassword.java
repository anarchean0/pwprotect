package pwprotect.model;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.modes.OFBBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.util.encoders.Base64;

public class PlainPassword {

    private EncryptedPassword encryptedPassword;
    private SecureRandom random;
    private String passwordCache;
    private KeyParameter key;

    public PlainPassword(EncryptedPassword encryptedPassword, KeyParameter key) {
        try {
            this.random = SecureRandom.getInstance("NativePRNG");
        } catch (NoSuchAlgorithmException ex) {
            this.random = new SecureRandom();
        }

        this.encryptedPassword = encryptedPassword;
        this.key = key;

        if (this.encryptedPassword.getValue() != null) {
            loadPassword(this.encryptedPassword.getValue());
        }
    }

    private void savePassword(String password) {
        final byte[] iv = new byte[16];
        final OFBBlockCipher ofb = new OFBBlockCipher(new AESEngine(), 128);

        final byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
        final byte[] encryptedBytes = new byte[passwordBytes.length];

        this.random.nextBytes(iv);

        ofb.init(true, new ParametersWithIV(key, iv));
        ofb.processBytes(passwordBytes, 0, passwordBytes.length, encryptedBytes, 0);

        // Stored new password
        final String encrypted = Base64.toBase64String(encryptedBytes);
        final String iv64 = Base64.toBase64String(iv);

        encryptedPassword.setValue(encrypted);
        encryptedPassword.setIv(iv64);
    }

    private void loadPassword(String passwordBase64) {
        final OFBBlockCipher ofb = new OFBBlockCipher(new AESEngine(), 128);

        final byte[] iv = Base64.decode(encryptedPassword.getIv());
        final byte[] pwBytes = Base64.decode(passwordBase64);
        final byte[] decryptedBytes = new byte[pwBytes.length];

        ofb.init(false, new ParametersWithIV(key, iv));
        ofb.processBytes(pwBytes, 0, pwBytes.length, decryptedBytes, 0);

        this.passwordCache = new String(decryptedBytes, StandardCharsets.UTF_8);
    }

    public String getPassword() {
        return passwordCache;
    }

    public void setPassword(String password) {
        passwordCache = password;
        savePassword(password);
    }

    public Long getId() {
        return encryptedPassword.getId();
    }

    public void setId(Long id) {
        encryptedPassword.setId(id);
    }

    public String getName() {
        return encryptedPassword.getName();
    }

    public void setName(String name) {
        encryptedPassword.setName(name);
    }

    public User getUser() {
        return encryptedPassword.getUser();
    }

    public void setUser(User user) {
        encryptedPassword.setUser(user);
    }

    public String getTagList() {
        return encryptedPassword.getTagList();
    }

    public void setTagList(String tagList) {
        encryptedPassword.setTagList(tagList);
    }

    public EncryptedPassword getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(EncryptedPassword encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }
}
