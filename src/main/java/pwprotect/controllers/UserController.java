package pwprotect.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import pwprotect.model.CryptoContext;
import pwprotect.model.User;
import pwprotect.views.LoginView;
import pwprotect.views.PasswordListView;
import pwprotect.views.UserView;
import pwprotect.views.Views;

public class UserController implements ActionListener {

    private Views views;
    private SessionFactory sessionFactory;

    public UserController(Views views, SessionFactory sessionFactory) {
        this.views = views;
        this.sessionFactory = sessionFactory;

        views.getLogin().setController(this);
        views.getUser().setController(this);
    }

    public void login(String user, String password) {
        Session s = sessionFactory.openSession();

        try {
            CriteriaBuilder builder = s.getCriteriaBuilder();
            CriteriaQuery<User> criteria = builder.createQuery(User.class);
            Root<User> root = criteria.from(User.class);
            criteria.select(root);
            criteria.where(builder.equal(root.get("name"), user));

            List<User> users = s.createQuery(criteria).getResultList();
            if (users.size() != 1) {
                JOptionPane.showMessageDialog(views.getLogin(), "User doesn't exists");
                return;
            }

            CryptoContext ctx = new CryptoContext(users.get(0));
            if (!ctx.checkPassword(password)) {
                JOptionPane.showMessageDialog(views.getLogin(), "User doesn't exists");
                return;
            }

            ctx.openContext(password);
            PasswordController pwCtl = new PasswordController(views, users.get(0), ctx, sessionFactory);
            PasswordListView view = views.getPasswordList();
            views.getLogin().setVisible(false);
            view.setController(pwCtl);
            view.setVisible(true);
            pwCtl.list(null);
        } finally {
            s.close();
        }
    }

    public void save(User user) {
        Session s = sessionFactory.openSession();
        Transaction tx = null;

        try {
            tx = s.beginTransaction();
            s.saveOrUpdate(user);
            tx.commit();

            UserView view = views.getUser();
            view.setVisible(false);
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
                JOptionPane.showMessageDialog(views.getLogin(), "Existing user, choose another name!");
            }
        } finally {
            s.close();
        }
    }

    public void delete(User user) {
        Session s = sessionFactory.openSession();
        Transaction tx = null;

        UserView userView = views.getUser();
        PasswordListView pwListView = views.getPasswordList();
        LoginView loginView = views.getLogin();

        try {
            userView.setVisible(false);

            tx = s.beginTransaction();
            s.delete(user);
            tx.commit();

            pwListView.setPasswords(Collections.emptyList());
            pwListView.setVisible(false);

            loginView.setVisible(true);
        } catch (Exception ex) {
            tx.rollback();
            
            ex.printStackTrace(System.err);
        } finally {
            s.close();
        }
    }

    public void create() {
        UserView userView = views.getUser();
        userView.setUser(new User());
        userView.setController(this);

        userView.setVisible(true);
        userView.update();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e.getActionCommand());
        if (e.getActionCommand().equalsIgnoreCase("login")) {
            login(views.getLogin().getJtf1().getText(), views.getLogin().getJtf2().getText());
        }

        if (e.getActionCommand().equalsIgnoreCase("create")) {
            create();
        }

        if (e.getActionCommand().equalsIgnoreCase("save")) {
            save(views.getUser().getUser());
        }

        if (e.getActionCommand().equalsIgnoreCase("delete")) {
            delete(views.getUser().getUser());
        }

        if (e.getActionCommand().equalsIgnoreCase("cancel")) {
            views.getUser().setVisible(false);
        }
    }
}
