package pwprotect;

import javax.swing.JFrame;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import pwprotect.controllers.UserController;
import pwprotect.views.LoginView;
import pwprotect.views.Views;

public class Application {

    private Views views;
    private SessionFactory factory;
    private UserController userController;

    public Application() {
        this.views = new Views();
        try {
            factory = new Configuration().configure("pwprotect/model/hibernate.cfg.xml").buildSessionFactory();
        } catch (Throwable e) {
            throw e;
        }
    }

    public void run() {
        this.views = new Views();

        LoginView login = this.views.getLogin();
        login.setController(this.userController);
        login.setVisible(true);
        login.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.userController = new UserController(this.views, factory);
    }

    public static void main(String[] args) {
        new Application().run();
    }
}
