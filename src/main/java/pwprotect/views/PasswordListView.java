package pwprotect.views;

import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import pwprotect.controllers.PasswordController;
import pwprotect.model.EncryptedPassword;

public class PasswordListView extends JFrame {

    private PasswordController controller;
    private List<EncryptedPassword> passwords;

    JButton jbtn1 = new JButton("Add");
    JButton jbtn2 = new JButton("Remove");
    JButton jbtn3 = new JButton("Edit User");
    JButton jbtn4 = new JButton("Edit Password");
    JButton jbtn5 = new JButton("Copy");

    JTextField jtf1 = new JTextField("", 40);

    JPanel jpn1 = new JPanel();
    JTable jtbl1 = new JTable();
    JScrollPane jscp1 = new JScrollPane(jtbl1);

    public PasswordListView() {
        this.setLayout(new FlowLayout());
        this.setSize(500, 550);

        jbtn1.setActionCommand("AdicionarNovoUsuario");
        this.getContentPane().add(jbtn1);
        jbtn2.setActionCommand("ExcluirUsuario");
        this.getContentPane().add(jbtn2);
        jbtn3.setActionCommand("EditarUsuario");
        this.getContentPane().add(jbtn3);
        jbtn4.setActionCommand("EditarSenha");
        this.getContentPane().add(jbtn4);
        jbtn5.setActionCommand("Copiar");
        this.getContentPane().add(jbtn5);
        this.getContentPane().add(jtf1);

        jpn1.add(jscp1);
        this.getContentPane().add(jpn1);

        jtbl1.setDefaultEditor(Object.class, null);
        DefaultTableModel dtm = (DefaultTableModel) jtbl1.getModel();
        dtm.addColumn("Name");
        dtm.addColumn("Tags");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        jbtn1.addActionListener(event -> controller.create());
        jbtn2.addActionListener(event -> {
            int selected = jtbl1.getSelectedRow();

            if (selected < 0) {
                JOptionPane.showMessageDialog(this, "Nenhuma senha selectionada");
                return;
            }

            controller.delete(passwords.get(selected));
        });
        
        jbtn3.addActionListener(event -> {
            controller.editUser();
        });

        jbtn4.addActionListener(event -> {
            int selected = jtbl1.getSelectedRow();

            if (selected < 0) {
                JOptionPane.showMessageDialog(this, "Nenhuma senha selectionada");
                return;
            }

            controller.edit(passwords.get(selected));
        });

        jbtn5.addActionListener(event -> {
            int selected = jtbl1.getSelectedRow();

            if (selected < 0) {
                JOptionPane.showMessageDialog(this, "Nenhuma senha selectionada");
                return;
            }
            
            controller.copyToClipboard(passwords.get(selected));
        });
        
        jtf1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                controller.list(jtf1.getText());
            }
        });
    }

    public void update() {
        DefaultTableModel dtm = (DefaultTableModel) jtbl1.getModel();

        // limpa a jtable
        while (dtm.getRowCount() > 0) {
            dtm.removeRow(0);
        }

        for (int i = 0; passwords != null && i < passwords.size(); i++) {
            EncryptedPassword p = passwords.get(i);
            if (p != null) {
                Object[] linhaTabela = new Object[2];
                linhaTabela[0] = p.getName();
                linhaTabela[1] = p.getTagList();

                dtm.addRow(linhaTabela);
            }
        }
    }

    public PasswordController getController() {
        return controller;
    }

    public void setController(PasswordController controller) {
        this.controller = controller;
    }

    public List<EncryptedPassword> getPasswords() {
        return passwords;
    }

    public void setPasswords(List<EncryptedPassword> passwords) {
        this.passwords = passwords;
    }

    public JButton getJbtn1() {
        return jbtn1;
    }

    public JButton getJbtn2() {
        return jbtn2;
    }

    public JButton getJbtn3() {
        return jbtn3;
    }

    public JTable getJtbl1() {
        return jtbl1;
    }
}
