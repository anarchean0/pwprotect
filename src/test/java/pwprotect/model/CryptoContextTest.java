package pwprotect.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 *
 * @author janil.garcia
 */
public class CryptoContextTest {

    @Test
    public void testChangePassword() {
        User user = new User();
        CryptoContext context = new CryptoContext(user);

        context.changePassword(null, "passworda");
        assertTrue("User password not is passworda", context.checkPassword("passworda"));

        context.changePassword("passworda", "passwordb");
        assertFalse("Password did not changed", context.checkPassword("passworda"));

        assertTrue("Password change was invalid", context.checkPassword("passwordb"));

        EncryptedPassword encrypted = new EncryptedPassword();
        encrypted.setName("Password 1");
        encrypted.setTagList("");

        context.openContext("passwordb");

        PlainPassword plain = context.decryptPassword(encrypted);
        plain.setPassword("password1");

        assertEquals("password1", plain.getPassword());

        context.closeContext();
        context.changePassword("passwordb", "passworda");
        context.openContext("passworda");

        plain = context.decryptPassword(encrypted);
        assertEquals("password1", plain.getPassword());
    }

    @Test
    public void testOpenContext() {
        final String pw = "mycoolpw";
        User user = new User();
        CryptoContext context = new CryptoContext(user);
        context.changePassword(null, pw);

        assertFalse(context.isOpen());
        context.openContext(pw);

        assertTrue(context.isOpen());
        context.closeContext();

        assertFalse(context.isOpen());
    }
}
